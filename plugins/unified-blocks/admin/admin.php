<?php

add_filter('plugin_action_links_unified-blocks/unified-blocks.php', 'unifiedblocks_plugin_settings_link');

// create custom plugin settings menu
add_action('admin_menu', 'unifiedblocks_create_menu');
//call register settings function
add_action('admin_init', 'unifiedblocks_register_settings');

register_activation_hook(plugin_dir_path(__FILE__).'/system.php', 'unifiedblocks_activate');
register_deactivation_hook(plugin_dir_path(__FILE__).'/system.php', 'unifiedblocks_deactivation');

// function unifiedblocks_plugin_settings_link($links)
// { 
//   	$settings_link = '<a href="options-general.php?page=unifiedblocks_settings">'.esc_html__('Settings').'</a>';
//   	array_unshift($links, $settings_link);
//   	return $links;
// }

function unifiedblocks_create_menu()
{
	//create new top-level menu
	//add_menu_page(esc_html__('Unified Blocks').' '.esc_html__('Settings'), esc_html__('Unified Blocks'), 'manage_options', 'unifiedblocks_settings', 'unifiedblocks_settings_page', 'dashicons-layout');
	// add_submenu_page('options-general.php', esc_html__('Unified Blocks'), esc_html__('Unified Blocks'), 'manage_options', 'unifiedblocks_settings', 'unifiedblocks_settings_page');	
	
}

function unifiedblocks_register_settings()
{
	//register our settings
	register_setting('unifiedblocks-settings-group', 'unifiedblocks_options');
}

function unifiedblocks_settings_page()
{
	include_once(plugin_dir_path(__FILE__).'/views/settings.php');
}


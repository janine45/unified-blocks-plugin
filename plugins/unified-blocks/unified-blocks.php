<?php
/*
Plugin Name: Unified Blocks
Plugin URI: http://localhost/unifiedblocks/unifiedblocks
Description: This is my parent plugin
Version: 1.0.1-rc
Author: admin
Author URI: http://localhost/unifiedblocks/
Text Domain: unifiedblocks
Domain Path: /languages
Generator: WordPress Plugin Creator
Generator URI: http://blog.paramonovav.com/builder/wp_plugin.php
*/

if (is_admin())
{
	include_once(plugin_dir_path(__FILE__).'/admin/admin.php');
}

function unifiedblocks_load_textdomain()
{
	load_plugin_textdomain('unifiedblocks', false, dirname(plugin_basename(__FILE__)) . '/languages');
}	
add_action('plugins_loaded', 'unifiedblocks_load_textdomain');

function unifiedblocks_get_option($option, $default = false)
{
	static $unifiedblocks_options;

    if (is_null($unifiedblocks_options))
    {
    	$unifiedblocks_options = is_multisite()? get_site_option('unifiedblocks_options', $default): get_option('unifiedblocks_options', $default);
    }

    if (isset($unifiedblocks_options[$option]))
    {
    	return $unifiedblocks_options[$option];
    }
    
    return $default;
}

<?php

add_filter('plugin_action_links_unified-block-plugin1/unified-block-plugin1.php', 'unifiedblockplugin1_plugin_settings_link');

// create custom plugin settings menu
add_action('admin_menu', 'unifiedblockplugin1_create_menu');
//call register settings function
add_action('admin_init', 'unifiedblockplugin1_register_settings');

register_activation_hook(plugin_dir_path(__FILE__).'/system.php', 'unifiedblockplugin1_activate');
register_deactivation_hook(plugin_dir_path(__FILE__).'/system.php', 'unifiedblockplugin1_deactivation');

function unifiedblockplugin1_plugin_settings_link($links)
{ 
  	$settings_link = '<a href="options-general.php?page=unifiedblockplugin1_settings">'.esc_html__('Settings').'</a>';
  	array_unshift($links, $settings_link);
  	return $links;
}

function unifiedblockplugin1_create_menu()
{
	//create new top-level menu
	// add_menu_page(esc_html__('Unified Block Plugin1').' '.esc_html__('Settings'), esc_html__('Unified Block Plugin1'), 'manage_options', 'unifiedblockplugin1_settings', 'unifiedblockplugin1_settings_page', plugins_url('../assets/icon/box.png', __FILE__));
	//add_submenu_page('options-general.php', esc_html__('Unified Block Plugin1'), esc_html__('Unified Block Plugin1'), 'manage_options', 'unifiedblockplugin1_settings', 'unifiedblockplugin1_settings_page');	
}

function unifiedblockplugin1_register_settings()
{
	//register our settings
	register_setting('unifiedblockplugin1-settings-group', 'unifiedblockplugin1_options');
}

function unifiedblockplugin1_settings_page()
{
	include_once(plugin_dir_path(__FILE__).'/views/settings.php');
}


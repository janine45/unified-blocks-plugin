<style> .indent {padding-left: 0em;} label{display:block;} </style>
<div class="wrap">
<h2><?php esc_html_e('Unified Block Plugin1'); ?> <?php esc_html_e('Settings'); ?></h2>
<form method="post" action="options.php">
    <?php settings_fields('unifiedblockplugin1-settings-group'); ?>
    <h3><?php esc_html_e('My First Settings Group', 'unifiedblockplugin1'); ?></h3>
    <ul>
        <li>
            <label><strong><?php esc_html_e('My Option label', 'unifiedblockplugin1'); ?></strong></label>
            <input type="text" name="unifiedblockplugin1_options[option_1]" value="<?php echo unifiedblockplugin1_get_option('option_1'); ?>"/>
            <p class="indent"><?php esc_html_e('description', 'unifiedblockplugin1'); ?></p>
        </li>
    </ul>    
    <p class="submit">
    <input type="hidden" name="unifiedblockplugin1_options[db_version]" value="1"/>
    <input type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
    </p>
</form>
</div>
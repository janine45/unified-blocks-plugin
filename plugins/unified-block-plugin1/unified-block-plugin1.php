<?php
/*
Plugin Name: Unified Block Plugin1
Plugin URI: http://localhost/unifiedblocks/unifiedblock1plugin
Description: This is my second plugin
Version: 1.0.1-rc
Author: admin
Author URI: http://localhost/unifiedblocks/
Text Domain: unifiedblockplugin1
Domain Path: /languages
Generator: WordPress Plugin Creator
Generator URI: http://blog.paramonovav.com/builder/wp_plugin.php
*/

if (is_admin())
{
	include_once(plugin_dir_path(__FILE__).'/admin/admin.php');
}

function unifiedblockplugin1_load_textdomain()
{
	load_plugin_textdomain('unifiedblockplugin1', false, dirname(plugin_basename(__FILE__)) . '/languages');
}	
add_action('plugins_loaded', 'unifiedblockplugin1_load_textdomain');

function unifiedblockplugin1_get_option($option, $default = false)
{
	static $unifiedblockplugin1_options;

    if (is_null($unifiedblockplugin1_options))
    {
    	$unifiedblockplugin1_options = is_multisite()? get_site_option('unifiedblockplugin1_options', $default): get_option('unifiedblockplugin1_options', $default);
    }

    if (isset($unifiedblockplugin1_options[$option]))
    {
    	return $unifiedblockplugin1_options[$option];
    }
    
    return $default;
}

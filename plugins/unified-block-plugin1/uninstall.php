<?php 

    if(!defined('ABSPATH') && !defined('WP_UNINSTALL_PLUGIN'))
    {
        exit;
    }

    delete_option('unifiedblockplugin1_options');
        
    if (is_multisite())
    {
    	// For site options in multisite
        delete_site_option('unifiedblockplugin1_options');
    }

    //drop a custom db table
    //global $wpdb;
    //$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}unifiedblockplugin1_table");

    //note in multisite looping through blogs to delete options on each blog does not scale. You'll just have to leave them.